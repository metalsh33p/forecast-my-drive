var weatherWaypoints = [],
    tripSegTime,
    waypoint = {};

function addWaypoint(segToAdd, timeOfRequest, duration) {
  
  waypoint = {};

  waypoint.lat = segToAdd.coords.lat;
  waypoint.lng = segToAdd.coords.lng;
  waypoint.duration = timeOfRequest + duration;

  weatherWaypoints.push(waypoint);

}

function init() {
  weatherWaypoints = [];
  tripSegTime = 0;
  waypoint = {};
}

exports.processLocations = function(segments, timeOfRequest){
  
  return new Promise(function(resolve, reject){

    if (!segments || !Array.isArray(segments)) {
      reject(new Error('Cannot make weather api request, missing segments'));
    }

    if (typeof timeOfRequest != 'number') {
      reject( new Error('Cannot process locations- incorrect or missing data passed in') );
    }

    init();

    var i = 0,
        segmentDuration,
        waypoints = segments.slice(),
        locations = waypoints.shift(),
        savedDiff,
        savedDuration,
        savedSegment,
        compDiff;

    tripSegTime = timeOfRequest;

    do {

      segmentDuration = 0;

      savedDuration = waypoints[0].duration;
      savedDiff = Math.abs( waypoints[0].coords.lat - locations[i].latLng.lat ) + Math.abs( waypoints[0].coords.lng - locations[i].latLng.lng );
      savedSegment = waypoints[0];

      for (var j = 1; j < waypoints.length; j++) {

        segmentDuration += parseInt(waypoints[j].duration);

        compDiff = Math.abs( waypoints[j].coords.lat - locations[i].latLng.lat ) + Math.abs( waypoints[j].coords.lng - locations[i].latLng.lng )

        if ( compDiff < savedDiff ) {
          savedDuration = segmentDuration;
          savedDiff = compDiff;
          savedSegment = waypoints[j];
        }

      }

      addWaypoint({
        coords: {
          lat: locations[i].latLng.lat,
          lng: locations[i].latLng.lng
        }
      }, timeOfRequest, savedDuration);

      i++;

    } while (i < locations.length);

    resolve(weatherWaypoints);

  });
}

exports.generateLocations = function (segments, timeOfRequest){
  return new Promise(function(resolve, reject){

    if (!segments || !Array.isArray(segments)) {
      reject(new Error('Cannot make weather api request, missing segments'));
    }

    if (!timeOfRequest || typeof timeOfRequest != 'number') {
      reject( new Error('Cannot process locations- incorrect or missing data passed in') );
    }

    init();

    addWaypoint(segments[1], timeOfRequest, parseInt(segments[1].duration));
    addWaypoint(segments[2], timeOfRequest, parseInt(segments[2].duration));
    addWaypoint(segments[3], timeOfRequest, parseInt(segments[3].duration));
    addWaypoint(segments[4], timeOfRequest, parseInt(segments[4].duration));
    addWaypoint(segments[5], timeOfRequest, parseInt(segments[5].duration));
    resolve(weatherWaypoints);

  });
}