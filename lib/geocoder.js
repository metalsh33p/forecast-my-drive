var https = require('https');

exports.reverseGeocode = function(lat, lng) {
  return new Promise(function(resolve, reject){

    if (!lat) {
      reject(new Error('Couldn\'t make reverseGeocode request, lat is missing'));
    }
    if (!lng) {
      reject(new Error('Couldn\'t make reverseGeocode request, lng is missing'));
    }
    https.get(`https://geocoding.geo.census.gov/geocoder/geographies/coordinates?x=${lng}&y=${lat}&benchmark=4&vintage=4&layers=28,84,86&format=json`, (res) => {
      const { statusCode } = res;
      const contentType = res.headers['content-type'];

      if (statusCode !== 200) {
        // consume response data to free up memory
        res.resume();
        reject(new Error(`Request Failed. Status Code: ${statusCode}`));
      } else if (!/^application\/json/.test(contentType)) {
        res.resume();
        reject(new Error(`Invalid content-type. Expected application/json but received ${contentType}`));
      }

      res.setEncoding('utf8');
      let rawData = '';
      res.on('data', (chunk) => { rawData += chunk; });
      res.on('end', () => {
        try {

          var reversedGeoData = JSON.parse(rawData);
          
          var incorporatedPlaces = reversedGeoData.result.geographies["Incorporated Places"],
              stateName = reversedGeoData.result.geographies.States[0].NAME;
          if (incorporatedPlaces.length > 0) {
            resolve(`${incorporatedPlaces[0].BASENAME}, ${stateName}`);
          } else {
            resolve(`${reversedGeoData.result.geographies.Counties[0].NAME}, ${stateName}`);
          }

        } catch (e) {
          reject(new Error(`Geocoder ran into an error while parsing data: ${e.message}`));
        }
      });
    }).on('error', (e) => {
      reject(new Error(`Geocoder ran into an error: ${e.message}`));
    });
  });
}