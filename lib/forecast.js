var numeral = require('numeral');

exports.fillForecastResArray = function (forecastResponses) {
  return new Promise(function(resolve, reject){
    if ( !forecastResponses || !Array.isArray(forecastResponses) ) {
      reject(new Error("Forecast responses are empty"));
    }
    var response,
        resWeatherPoints = [],
        j;
    for (var i = 0; i < forecastResponses.length; i++) {
      j = 0;
      response = forecastResponses[i];
      while(response.waypointTime < response.hourly.data[j].time && i < response.hourly.data.length) {
        i++;
      }
      if (response.hourly.data[j + 1]) {
        if (Math.abs(response.waypointTime - response.hourly.data[j].time) < Math.abs(response.waypointTime - response.hourly.data[j + 1].time)) {
          i++;
        }
      }
      var forecastResponse = response.hourly.data[j];
      resWeatherPoints.push({
        coords: {
          lat: response.latitude,
          lng: response.longitude
        },
        icon: forecastResponse.icon.toUpperCase(),
        summary: forecastResponse.summary,
        temp: `${numeral(forecastResponse.temperature).format('0')}F`,
        feelsLike: `${numeral(forecastResponse.apparentTemperature).format('0')}F`,
        visibility: `${numeral(forecastResponse.visibility).format('0.00')}m`,
        precip: numeral(forecastResponse.precipProbability).format('0%'),
        wind: `${numeral(forecastResponse.windSpeed).format('0')}mph`
      }); 
    }
    resolve(resWeatherPoints);
  });
}