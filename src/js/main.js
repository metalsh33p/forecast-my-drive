var map,
    dir,
    WeatherIcon,
    weatherLayerGroup = null,
    routeLayerGroup = null,
    weatherPoints,
    weatherPointsLength,
    resultCount,
    dotCount = 1,
    $resultDot = $("#result-dot-0").clone().removeClass("fa-circle").addClass("fa-circle-o"),
    routeShapeObj, 
    skyconAdded = false,
    skycons = new Skycons({
      "color": "#16425B",
      "resizeClear": true
    });

// This function initializes the maps and sets up the main submit event listener
window.onload = init();  

function init() {

    map = L.map('map', {
        layers: MQ.mapLayer(),
        center: [ 41.85, -87.65 ],
        zoom: 12,
        zoomControl: false
    });

    WeatherIcon = L.Icon.extend({
        options: {
            iconSize:     [36, 53], // size of the icon
            iconAnchor:   [18, 53], // point of the icon which will correspond to marker's location
        }
    });

    weatherLayerGroup = L.layerGroup();

    (function() {
        var burger = document.querySelector('.nav-toggle');
        var menu = document.querySelector('.nav-menu');
        burger.addEventListener('click', function() {
            burger.classList.toggle('is-active');
            menu.classList.toggle('is-active');
        });
    })(); 

    (function() {
        var navHeight = $("#top").height();
        if ($(document).width() <= 768) {
            $("#content-container").css("top", navHeight);
        }
    })();

    var $departBtn = $("#depart-button");
    $departBtn.on('click', function(event){
        event.preventDefault();
        $departBtn.text("at");
        $("#date-container").css("display", "block");
    })

    $("#waypoint-btn").on('click', addWaypoint);

    $(".del-waypoint").on('click', deleteWaypoint);

    $("#submit-btn").on('click', submitRouteForm);
    $("#nav-header").on("click", toggleNavCard);
    $("#weather-header").on("click", toggleWeatherCard);
    $("#to-result-right").on("click", resultToRight);
    $("#to-result-left").on("click", resultToLeft);

}

function submitRouteForm(event) {

    event.preventDefault();

    var filledCheckRgx = /\S/i,
        errExists = false;

    if ( !$('#from-city').val().match(filledCheckRgx) ) {
        displayError($("#from-city"), $("#from-error"), 'FROM field required.');
        errExists = true;
    } else {
        $("#from-city").removeClass('is-danger');
        $("#from-error").addClass('hide');
    }

    if ( !$('#to-city').val().match(filledCheckRgx) ) {
        displayError($("#to-city"), $("#to-error"), 'TO field required.');
        errExists = true;
    } else {
        $("#to-city").removeClass('is-danger');
        $("#to-error").addClass('hide');
    }

    if ( $('#from-city').val().toLowerCase().replace(/ /g, '') === $('#to-city').val().toLowerCase().replace(/ /g, '') ) {
        displayError($("#to-city"), $("#to-error"), 'TO field must be different than FROM field.');
        errExists = true;
    } else {
        $("#to-city").removeClass('is-danger');
        $("#to-error").addClass('hide');
    }

    if (errExists) {
        return;
    }

    if (typeof routeLayerGroup !== 'undefined' && routeLayerGroup !== null) {
        routeLayerGroup.clearLayers();
    }

    if (typeof weatherLayerGroup !== 'undefined' && weatherLayerGroup !== null) {
        weatherLayerGroup.clearLayers();
    }

    $("#error-field").addClass('hide');
    $("#submit-btn").text("Loading");
    setupAndCallRoute();

}

function addWaypoint(event) {
    event.preventDefault();
    var $hideWaypoint = $(".hide-waypoint");
    if ($hideWaypoint.length === 1){
        $hideWaypoint.removeClass("hide-waypoint");
    } else {
        var $waypoints = $(".waypoint");
        if ($waypoints.length < 3) {
            var $newWaypoint = $waypoints.eq(0).clone(true);
            var $newInput = $newWaypoint.find("input:first");
            $newInput.val('');
            $("#waypoint-grp").append($newWaypoint);
        }
    }
}

function deleteWaypoint(event) {
    event.preventDefault(); 
    if ($(".waypoint").length === 1) {
        var $waypointGroup = $("#waypoint-grp");
        $waypointGroup.addClass("hide-waypoint");
        $waypointGroup.find("input:first").val('');
    } else {
        $(this).closest(".waypoint").remove();
    }
}

// This function will call the weather api and then display the results
function setupAndCallRoute() {

    var routeRequest = {
        locations: [],
        options: {
            narrativeType: 'none',
            routeType: 'FASTEST',
            drivingStyle: 2,
            fullShape: true,
            narrativeType: 'none',
            locale: 'en_US' 
        }
    };

    routeRequest.locations.push($("#from-city").val());
    
    var $waypointTexts = $(".waypoint-text");
    for(var i = 0; i < $waypointTexts.length; i++) {
       var waypoint_val = $waypointTexts.eq(i).val();
        if (waypoint_val !== '') {
            routeRequest.locations.push(waypoint_val);
        }
    }

    routeRequest.locations.push($("#to-city").val());

    dir = MQ.routing.directions().on('success', function(data) {
            if (data.route.locations) {
                processRouteResults(data);
            }
        })
        .on('error', function() {
            $("#submit-btn").button('reset');
            $("#error-field").removeClass('hide');
            $("#error-field").text("Error occured while getting directions.");
        });

    dir.route(routeRequest);

    routeLayerGroup = MQ.routing.routeLayer({
        directions: dir,
        fitBounds: true,
    });

    map.addLayer(routeLayerGroup);

// TODO add time request
    // var timeOfRequest = $("#depart-input").val();
    // if (timeOfRequest !== '') {
    //     timeOfRequest = Math.floor(Date.parse(timeOfRequest)/1000);
    // } else {
    //     timeOfRequest = new Date();
    //     timeOfRequest = Math.floor(timeOfRequest.getTime()/1000) - (timeOfRequest.getTimezoneOffset() * 60);
    // }
// END TODO
}

function processRouteResults(data) {

    var timeOfRequest = new Date(),
        timeOfRequest = Math.floor(timeOfRequest.getTime()/1000) - (timeOfRequest.getTimezoneOffset() * 60);

    var weatherRequest = {
        time_of_request: timeOfRequest,
        trip_duration: data.route.time,
        weather_points: []
    };

    var  shapePoints = data.route.shape.shapePoints;
    weatherRequest.weather_points.push(data.route.locations);

    var routeLength = Math.round((shapePoints.length) / 4);
    var routePointTime = Math.round(parseInt(weatherRequest.trip_duration) / 4);

    weatherRequest.weather_points.push({ coords: { lat: shapePoints[0].lat, lng: shapePoints[0].lng }, duration: 0 });
    weatherRequest.weather_points.push({ coords: { lat: shapePoints[routeLength].lat, lng: shapePoints[routeLength].lng }, duration: routePointTime });
    weatherRequest.weather_points.push({ coords: { lat: shapePoints[routeLength * 2].lat, lng: shapePoints[routeLength * 2].lng }, duration: (routePointTime * 2) });
    weatherRequest.weather_points.push({ coords: { lat: shapePoints[routeLength * 3].lat, lng: shapePoints[routeLength * 3].lng }, duration: (routePointTime * 3) });
    weatherRequest.weather_points.push({ coords: { lat: shapePoints[shapePoints.length - 1].lat, lng: shapePoints[shapePoints.length - 1].lng }, duration: weatherRequest.trip_duration });

    callWeatherAPI(weatherRequest);
}

function callWeatherAPI(weatherReq) {
    $.ajax({
        method: "POST",
        url: "/api/weather",
        data: weatherReq
    }).done(function(weatherRes) {

        weatherPoints = weatherRes.weather_points;
        weatherPointsLength = weatherPoints.length;
        resultCount = 0;
        loadResultDots();
        loadRoutes();

        displayResults();
        toggleNavCard();
        // toggleWeatherCard();
        $("#results-card").removeClass("hide-results");
        $("#weather-header").removeClass("hide-results");

        var markers = routeLayerGroup.markers;
        for(var j = 0; j < markers.length; j++) {
            routeLayerGroup.removeLayer(markers[j]._leaflet_id);
        }

        var weatherMarker,
            weatherMarkerPath;
        for(var i = 0; i < weatherPointsLength; i++) {
            weatherMarkerPath = '/images/marker/' + weatherPoints[i].icon + '-marker.png';
            weatherMarker = new WeatherIcon({iconUrl: weatherMarkerPath});
            weatherLayerGroup.addLayer(L.marker([weatherPoints[i].coords.lat, weatherPoints[i].coords.lng],{icon: weatherMarker}));
        }
        map.addLayer(weatherLayerGroup);

        dir.removeEventListener('success');

        processResultsCalled = false;

    }).fail(function(){
        console.log('there was an error making the weather call!');
        $("#submit-btn").text("Forecast My Drive");
        $("#error-field").text("Error occured while getting forecasts.");
        $("#error-field").removeClass('hide');

        processResultsCalled = false;
    });
}

//TODO START MOVE TO MODULE
function loadRoutes() {
    skycons.remove("skycon");
    var weatherIcon = weatherPoints[resultCount].icon;
    weatherIcon = weatherIcon.replace(/-/g, '_');  
    skycons.set("skycon", Skycons[weatherIcon]);
    skycons.play();  
    $("#result-point").text(weatherPoints[resultCount].locationName);
    $("#result-sum").text(weatherPoints[resultCount].summary);
    $("#result-temp").text(weatherPoints[resultCount].temp);
    $("#result-feels").text(weatherPoints[resultCount].feelsLike);
    $("#result-wind").text(weatherPoints[resultCount].wind);
    $("#result-precip").text(weatherPoints[resultCount].precip);
    $("#result-visibility").text(weatherPoints[resultCount].visibility);
}

function displayResults() {
    $("#submit-btn").text("Forecast My Drive");
    $("#results-card").removeClass("hide-results");
    $("#weather-header").removeClass("hide-results");
}

function toggleNavCard() {
  $("#nav-card").toggle();
  var $navAngle = $("#route-toggle");
  $navAngle.toggleClass("fa-angle-down");
  $navAngle.toggleClass("fa-angle-left");  
  $("#submit-btn").toggleClass("is-disabled");
}

function toggleWeatherCard() {
  $("#results-card").toggle();
  var $weatherAngle = $("#weather-toggle");
  $weatherAngle.toggleClass("fa-angle-down");
  $weatherAngle.toggleClass("fa-angle-left");  
}

function resultToRight() {
    changeCurrentDot();
    resultCount++;
    calculatePosition();
    changeNextDot();
    loadRoutes();
}

function resultToLeft() {
    changeCurrentDot();
    resultCount--;
    calculatePosition();
    changeNextDot();
    loadRoutes();
}

//TODO has to be a better way to handle this
function loadResultDots() {
    var $firstDot = $("#result-dot-0"),
        $dotContainer = $("#card-position"),
        $clonedDot,
        resultDotID;
    
    do {
        resultDotID = '#result-dot-' + dotCount;
        $(resultDotID).remove();
        dotCount--;
    } while (dotCount >= 1);

    if ($firstDot.hasClass("fa-circle-o")) {
        $firstDot.removeClass("fa-circle-o").addClass("fa-circle");
    }

    dotCount++;

    do {
        resultDotID = 'result-dot-' + dotCount;
        $clonedDot = $resultDot.clone().attr('id', resultDotID);
        $clonedDot.appendTo($dotContainer);
        dotCount++;
    } while (dotCount < weatherPointsLength);

    $("#card-position").removeClass("hide");
}

function changeCurrentDot() {
    var currentDotSelector = '#result-dot-' + (resultCount);
    $(currentDotSelector).toggleClass("fa-circle");
    $(currentDotSelector).toggleClass("fa-circle-o");
}

function changeNextDot() {
    var nextDotSelector = '#result-dot-' + resultCount;
    $(nextDotSelector).toggleClass("fa-circle-o");
    $(nextDotSelector).toggleClass("fa-circle");
}

function calculatePosition() {
    if (resultCount < 0) {
        resultCount = weatherPointsLength - 1;
    } else if (resultCount > (weatherPointsLength -1)) {
        resultCount = 0;
    }
}
function displayError($element, $errElement, errMsg) {
    $element.addClass('is-danger');
    $errElement.text(errMsg);
    $errElement.removeClass('hide');
}
//TODO END MOVE TO MODULE