var chai = require('chai'),
    should = chai.should(),
    chaiAsPromised = require('chai-as-promised'),
    response1 = require('./requests/forecast1_responses.json'),
    response2 = require('./requests/forecast2_responses.json'),
    forecast = require('../lib/forecast.js');

chai.use(chaiAsPromised);

describe('forecast', function(){

  describe('#fillForecastResArray()', function() { 

    var forecastResponses = response1.forecast_responses;

    it('should return an array', function() {
      return forecast.fillForecastResArray(forecastResponses).should.eventually.be.instanceof(Array);
    }); 

    it('should return an array with length > 0', function() {
      return forecast.fillForecastResArray(forecastResponses).should.eventually.not.be.empty;
    });

    it('should return an array of objects', function() {
      return forecast.fillForecastResArray(forecastResponses).then(function(waypoints){
        waypoints.should.not.be.empty;
        for (var i = 0; i < waypoints.length; i++) {
          waypoints[i].should.be.an('object');
        }
      });
    });

    it('should return an array of objects that each have a lat property', function() {
      return forecast.fillForecastResArray(forecastResponses).then(function(waypoints){
        for (var i = 0; i < waypoints.length; i++) {
          waypoints[i].coords.should.have.property('lat');
        }
      }); 
    });

    it('should return an array of objects that each have a lng property', function() {
      return forecast.fillForecastResArray(forecastResponses).then(function(waypoints){
        for (var i = 0; i < waypoints.length; i++) {
          waypoints[i].coords.should.have.property('lng');
        }
      }); 
    });

    it('should return an array of objects that each have an iconPath property', function() {
      return forecast.fillForecastResArray(forecastResponses).then(function(waypoints){
        for (var i = 0; i < waypoints.length; i++) {
          waypoints[i].should.have.property('icon');
        }
      }); 
    });

    it('should return an array of objects that each have a summary property', function() {
      return forecast.fillForecastResArray(forecastResponses).then(function(waypoints){
        for (var i = 0; i < waypoints.length; i++) {
          waypoints[i].should.have.property('summary');
        }
      }); 
    });

    it('should return an array of objects that each have a temp property', function() {
      return forecast.fillForecastResArray(forecastResponses).then(function(waypoints){
        for (var i = 0; i < waypoints.length; i++) {
          waypoints[i].should.have.property('temp');
        }
      }); 
    });

    it('should return an array of objects that each have a feelsLike property', function() {
      return forecast.fillForecastResArray(forecastResponses).then(function(waypoints){
        waypoints.should.not.be.empty;
        for (var i = 0; i < waypoints.length; i++) {
          waypoints[i].should.have.property('feelsLike');
        }
      }); 
    });

    it('should return an array of objects that each have a visibility property', function() {
      return forecast.fillForecastResArray(forecastResponses).then(function(waypoints){
        for (var i = 0; i < waypoints.length; i++) {
          waypoints[i].should.have.property('visibility');
        }
      }); 
    });

    it('should return an array of objects that each have a precip property', function() {
      return forecast.fillForecastResArray(forecastResponses).then(function(waypoints){
        waypoints.should.not.be.empty;
        for (var i = 0; i < waypoints.length; i++) {
          waypoints[i].should.have.property('precip');
        }
      }); 
    });

    it('should return an array of objects that each have a wind property', function() {
      return forecast.fillForecastResArray(forecastResponses).then(function(waypoints){
        for (var i = 0; i < waypoints.length; i++) {
          waypoints[i].should.have.property('wind');
        }
      }); 
    });

    it('should return an array of different values between consecutive calls', function() {

      return forecast.fillForecastResArray(forecastResponses)
        .then(function(waypoints){
          var firstCallWaypoints = waypoints;
          return forecast.fillForecastResArray(response2.forecast_responses)
            .then(function(secondCallWaypoints){
              var currentRes,
                  compareLat,
                  compareLng;

              for(var i = 0; i < firstCallWaypoints.length; i++) {
                currentRes = firstCallWaypoints[i];
                for(var j = 0; j < secondCallWaypoints.length; j++) {
                  compareLat = secondCallWaypoints[j].coords.lat;
                  compareLng = secondCallWaypoints[j].coords.lng;
                  currentRes.coords.lat.should.not.equal.compareLat;
                  currentRes.coords.lng.should.not.equal.compareLng;
                }
              }
            });
        });
      
    });

    it('should return a resolved promise', function() {
      return forecast.fillForecastResArray(forecastResponses).should.be.resolved;
    }); 

    it('should return a rejected promise when no parameter is passed', function() {
      return forecast.fillForecastResArray().should.be.rejected;
    }); 

    it('should return a rejected promise when a parameter other than an array is passed in', function() {
      return forecast.fillForecastResArray('this is not an array').should.be.rejected;
    }); 

  });

});