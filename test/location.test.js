var chai = require('chai'),
    should = chai.should(),
    chaiAsPromised = require('chai-as-promised'),
    location = require('../lib/location.js'),
    chi_madison_request = require('./requests/chi_madison_request.json'),
    omaha_dm_request = require('./requests/omaha_dm_request.json'),
    one_wypnt_request = require('./requests/one_waypoint_req.json'),
    two_wypnt_request = require('./requests/two_waypoint_req.json'),
    three_wypnt_request = require('./requests/three_waypoint_req.json');

chai.use(chaiAsPromised);

describe('location', function(){

  var timeOfRequest = new Date(),
      timeOfRequest = Math.floor(timeOfRequest.getTime()/1000) - (timeOfRequest.getTimezoneOffset() * 60);

  describe('#processLocations()', function() {

    it('should return an array', function() {
      return location.processLocations(one_wypnt_request.weather_points, timeOfRequest).should.eventually.be.instanceof(Array);
    });

    it('should return an array with length > 0', function() {
      return location.processLocations(one_wypnt_request.weather_points, timeOfRequest).should.eventually.not.be.empty;
    });

    it('should return an array with length < 6', function() {
      return location.processLocations(one_wypnt_request.weather_points, timeOfRequest).should.eventually.have.length.below(6);
    });

    it('should return an array with length of 3 when there is 1 waypoint', function() {
      return location.processLocations(one_wypnt_request.weather_points, timeOfRequest).should.eventually.have.lengthOf(3);
    });

    it('should return an array with length of 4 when there are 2 waypoints', function() {
      return location.processLocations(two_wypnt_request.weather_points, timeOfRequest).should.eventually.have.lengthOf(4);
    });

    it('should return an array with length of 5 when there are 3 waypoints', function() {
      return location.processLocations(three_wypnt_request.weather_points, timeOfRequest).should.eventually.have.lengthOf(5);
    });

    it('should return an array of objects', function() {
      return location.processLocations(one_wypnt_request.weather_points, timeOfRequest).then(function(waypoints){

        waypoints.should.not.be.empty;

        for (var i = 0; i < waypoints.length; i++) {
          waypoints[i].should.be.an('object');
        }

      });
    });

    it('should return an array of objects that each has a lat property', function(){ 
      return location.processLocations(one_wypnt_request.weather_points, timeOfRequest).then(function(waypoints){

        waypoints.should.not.be.empty;

        for (var i = 0; i < waypoints.length; i++) {
          waypoints[i].should.have.property('lat');
        }

      });
    });

    it('should return an array of objects whose lat property is a number', function(){ 
      return location.processLocations(one_wypnt_request.weather_points, timeOfRequest).then(function(waypoints){

        waypoints.should.not.be.empty;

        for (var i = 0; i < waypoints.length; i++) {
          waypoints[i].lat.should.be.a('number');
        }

      });
    });

    it('should return an array of objects that each has a lng property', function(){
      return location.processLocations(one_wypnt_request.weather_points, timeOfRequest).then(function(waypoints){

        waypoints.should.not.be.empty;

        for (var i = 0; i < waypoints.length; i++) {
          waypoints[i].should.have.property('lng');
        }

      });
    });

    it('should return an array of objects whose lng property is a number', function(){ 
      return location.processLocations(one_wypnt_request.weather_points, timeOfRequest).then(function(waypoints){

        waypoints.should.not.be.empty;

        for (var i = 0; i < waypoints.length; i++) {
          waypoints[i].lng.should.be.a('number');
        }

      });
    });

    it('should return an array of objects that each has a duration property', function(){
      return location.processLocations(one_wypnt_request.weather_points, timeOfRequest).then(function(waypoints){

        waypoints.should.not.be.empty;

        for (var i = 0; i < waypoints.length; i++) {
          waypoints[i].should.have.property('duration');
        }

      });
    });

    it('should return an array of objects whose duration property is a valid UTC time', function(){
      return location.processLocations(one_wypnt_request.weather_points, timeOfRequest).then(function(waypoints){

        waypoints.should.not.be.empty;

        for (var i = 0; i < waypoints.length; i++) {
          waypoints[i].duration.toString().should.have.lengthOf(10);
        }

      });
    });

    it('should return an array of objects whose lat, lng, and duration values are different from each other', function(){
      return location.processLocations(three_wypnt_request.weather_points, timeOfRequest).then(function(waypoints){

        var i = 0,
            currentWayLat,
            currentWayLng,
            currentWayDur,
            compareWayLat,
            compareWayLng,
            compareWayDur;
        
        while (i < waypoints.length) {
          currentWayLat = waypoints[i].lat;
          currentWayLng = waypoints[i].lng;
          currentWayDur = waypoints[i].duration;
          for(var j = 0; j < waypoints.length; j++) {
            compareWayLat = waypoints[j].lat;
            compareWayLng = waypoints[j].lng;
            compareWayDur = waypoints[j].duration;
            if (i != j) {
              currentWayLat.should.not.equal.compareWayLat;
              currentWayLng.should.not.equal.compareWayLng;
              currentWayDur.should.not.equal.compareWayDur;
            }
          }
          i++;
        }

      });
    });

    it('should return an array of different values between consecutive calls', function() {

      var waypointSetOne,
          waypointSetTwo;

      return location.processLocations(three_wypnt_request.weather_points, timeOfRequest)
        .then(function(firstWaypoints){
          waypointSetOne = firstWaypoints;
          return location.processLocations(two_wypnt_request.weather_points, timeOfRequest).then(function(secondWaypoints){
            waypointSetTwo = secondWaypoints;
          });
        })
        .then(function(){
          for(var i = 0; i < waypointSetOne.length; i++) {
            currentLat = waypointSetOne[i].lat;
            currentLng = waypointSetOne[i].lng;
            for(var j = 0; j < waypointSetTwo.length; j++) {
              compareLat = waypointSetTwo[j].lat;
              compareLng = waypointSetTwo[j].lng;
              currentLat.should.not.equal.compareLat;
              currentLng.should.not.equal.compareLng;
            }
          }
        });
      
    });

    it('should return an array whose first waypoint.lng should match the longitude of the location in the FROM field', function() {
      return location.processLocations(three_wypnt_request.weather_points, timeOfRequest).then(function(waypoints){
        waypoints[0].lng.should.equal(-87.712907);
      });
    });

    it('should return an array whose first waypoint.lat should match the latitude of the location in the FROM field', function() {
      return location.processLocations(three_wypnt_request.weather_points, timeOfRequest).then(function(waypoints){
        waypoints[0].lat.should.equal(43.75088);
      });
    });

    it('should return an array whose last waypoint.lng should match the longitude of the location in the TO field', function() {
      return location.processLocations(three_wypnt_request.weather_points, timeOfRequest).then(function(waypoints){
        waypoints[waypoints.length - 1].lng.should.equal(-97.11319);
      });
    });

    it('should return an array whose last waypoint.lat should match the latitude of the location in the TO field', function() {
      return location.processLocations(three_wypnt_request.weather_points, timeOfRequest).then(function(waypoints){
        waypoints[waypoints.length - 1].lat.should.equal(44.0072);
      });
    });

    it('should return a resolved promise', function() {
      return location.processLocations(three_wypnt_request.weather_points, timeOfRequest).should.be.resolved;
    }); 

    it('should return a rejected promise when no weather points parameter is passed', function() {
      return location.processLocations(timeOfRequest).should.be.rejected; 
    });

    it('should return a rejected promise when no timeOfRequest parameter is passed', function() {
      return location.processLocations(three_wypnt_request.weather_points).should.be.rejected; 
    }); 

    it('should return a rejected promise when a parameter other than an array is passed in for weather_points', function() {
      return location.processLocations('not an array', timeOfRequest).should.be.rejected;
    }); 

    it('should return a rejected promise when a parameter other than a number is passed in for timeOfRequest', function() {
      return location.processLocations(three_wypnt_request.weather_points, 'not a number').should.be.rejected;
    }); 

  });

  describe('#generateLocations()', function() {

    it('should return an array', function() {
      return location.generateLocations(chi_madison_request.weather_points, timeOfRequest).should.eventually.be.instanceof(Array);
    });

    it('should return an array with length > 0', function() {
      return location.generateLocations(chi_madison_request.weather_points, timeOfRequest).should.eventually.not.be.empty;
    });

    it('should return an array with length < 6', function() {
      return location.generateLocations(chi_madison_request.weather_points, timeOfRequest).should.eventually.have.length.below(6);
    });

    it('should return an array with length of 5', function() {
      return location.generateLocations(chi_madison_request.weather_points, timeOfRequest).should.eventually.have.lengthOf(5);
    });

    it('should return an array of objects', function() {
      return location.generateLocations(chi_madison_request.weather_points, timeOfRequest).then(function(waypoints){

        waypoints.should.not.be.empty;

        for (var i = 0; i < waypoints.length; i++) {
          waypoints[i].should.be.an('object');
        }

      });
    });

    it('should return an array of objects that each has a lat property', function(){ 
      return location.generateLocations(chi_madison_request.weather_points, timeOfRequest).then(function(waypoints){

        waypoints.should.not.be.empty;

        for (var i = 0; i < waypoints.length; i++) {
          waypoints[i].should.have.property('lat');
        }

      });
    });

    it('should return an array of objects that each has a lng property', function(){
      return location.generateLocations(chi_madison_request.weather_points, timeOfRequest).then(function(waypoints){

        waypoints.should.not.be.empty;

        for (var i = 0; i < waypoints.length; i++) {
          waypoints[i].should.have.property('lng');
        }

      });
    });

    it('should return an array of objects that each has a duration property', function(){
      return location.generateLocations(chi_madison_request.weather_points, timeOfRequest).then(function(waypoints){

        waypoints.should.not.be.empty;

        for (var i = 0; i < waypoints.length; i++) {
          waypoints[i].should.have.property('duration');
        }

      });
    });

// TODO come up with a better test for this
    it('should return an array of objects whose duration property is a valid UTC time', function(){
      return location.generateLocations(chi_madison_request.weather_points, timeOfRequest).then(function(waypoints){

        waypoints.should.not.be.empty;

        for (var i = 0; i < waypoints.length; i++) {
          waypoints[i].duration.toString().should.have.lengthOf(10);
        }

      });
    });

    it('should return an array of objects whose lat, lng, and duration values are different from each other', function(){
      return location.generateLocations(chi_madison_request.weather_points, timeOfRequest).then(function(waypoints){

        var i = 0,
            currentWayLat,
            currentWayLng,
            currentWayDur,
            compareWayLat,
            compareWayLng,
            compareWayDur;
        
        while (i < waypoints.length) {
          currentWayLat = waypoints[i].lat;
          currentWayLng = waypoints[i].lng;
          currentWayDur = waypoints[i].duration;
          for(var j = 0; j < waypoints.length; j++) {
            compareWayLat = waypoints[j].lat;
            compareWayLng = waypoints[j].lng;
            compareWayDur = waypoints[j].duration;
            if (i != j) {
              currentWayLat.should.not.equal.compareWayLat;
              currentWayLng.should.not.equal.compareWayLng;
              currentWayDur.should.not.equal.compareWayDur;
            }
          }
          i++;
        }

      });
    });

    it('should return an array of different values between consecutive calls', function() {

      var waypointSetOne,
          waypointSetTwo;

      return location.generateLocations(chi_madison_request.weather_points, timeOfRequest)
        .then(function(firstWaypoints){
          waypointSetOne = firstWaypoints;
          return location.generateLocations(omaha_dm_request.weather_points, timeOfRequest).then(function(secondWaypoints){
            waypointSetTwo = secondWaypoints;
          });
        })
        .then(function(){
          for(var i = 0; i < waypointSetOne.length; i++) {
            currentLat = waypointSetOne[i].lat;
            currentLng = waypointSetOne[i].lng;
            for(var j = 0; j < waypointSetTwo.length; j++) {
              compareLat = waypointSetTwo[j].lat;
              compareLng = waypointSetTwo[j].lng;
              currentLat.should.not.equal.compareLat;
              currentLng.should.not.equal.compareLng;
            }
          }
        });
      
    });

    it('should return an array whose first waypoint.lng should match the longitude of the location in the FROM field', function() {
      return location.generateLocations(chi_madison_request.weather_points, timeOfRequest).then(function(waypoints){
        waypoints[0].lng.should.equal('-87.624336');
      });
    });

    it('should return an array whose first waypoint.lat should match the latitude of the location in the FROM field', function() {
      return location.generateLocations(chi_madison_request.weather_points, timeOfRequest).then(function(waypoints){
        waypoints[0].lat.should.equal('41.875553');
      });
    });

    it('should return an array whose last waypoint.lng should match the longitude of the location in the TO field', function() {
      return location.generateLocations(chi_madison_request.weather_points, timeOfRequest).then(function(waypoints){
        waypoints[waypoints.length - 1].lng.should.equal('-97.113952');
      });
    });

    it('should return an array whose last waypoint.lat should match the latitude of the location in the TO field', function() {
      return location.generateLocations(chi_madison_request.weather_points, timeOfRequest).then(function(waypoints){
        waypoints[waypoints.length - 1].lat.should.equal('44.007132999999996');
      });
    });

    it('should return a resolved promise', function() {
      return location.generateLocations(chi_madison_request.weather_points, timeOfRequest).should.be.resolved;
    }); 

    it('should return a rejected promise when no weather points parameter is passed', function() {
      return location.generateLocations(timeOfRequest, chi_madison_request.trip_duration).should.be.rejected; 
    });

    it('should return a rejected promise when no timeOfRequest parameter is passed', function() {
      return location.generateLocations(chi_madison_request.weather_points).should.be.rejected; 
    }); 

    it('should return a rejected promise when a parameter other than an array is passed in for weather_points', function() {
      return location.generateLocations('not an array', timeOfRequest).should.be.rejected;
    }); 

    it('should return a rejected promise when a parameter other than a number is passed in for timeOfRequest', function() {
      return location.generateLocations(chi_madison_request.weather_points, 'not a number').should.be.rejected;
    });  
    
  });

});


