var express = require('express'),
    connect = require('connect'),
    compression = require('compression'),
    credentials = require('./credentials.js'),
    Forecast = require('forecast.io'),
    location = require('./lib/location.js'),
    geocoder = require('./lib/geocoder.js'),
    forecastRes = require('./lib/forecast.js'),
    createDOMPurify = require('dompurify'),
    jsdom = require('jsdom'),
    manifest = require('./public/rev-manifest.json');

// Set up sanitizer for weather api requests
var { JSDOM } = jsdom;
var window = (new JSDOM('', {
  features: {
    FetchExternalResources: false, // disables resource loading over HTTP / filesystem
    ProcessExternalResources: false // do not execute JS within script blocks
  }
})).window;
var DOMPurify = createDOMPurify(window);

// Begin Express
var app = express();
app.use(compression());

// set up handlebars view engine
var handlebarsOptions = {
  defaultLayout:'main',
  helpers: {
    section: function(name, options){
      if(!this._sections) this._sections = {};
      this._sections[name] = options.fn(this);
      return null;
    },
    vendorCSS: manifest["fmd-vendor-stylesheet.css"],
    customCSS: manifest["fmd-custom-stylesheet.css"],
    bundledJS: manifest["fmd-bundle.js"]
  },
}
// if ( app.get('env') === 'development' ) {
//   handlebarsOptions.helpers.bundledJS = manifest["main.js"];
// } else {
//   handlebarsOptions.helpers.bundledJS = manifest["fmd-bundle.js"];
// }

var handlebars = require('express-handlebars').create(handlebarsOptions);

app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');

app.set('port', process.env.PORT || 3000);

app.use(express.static(__dirname + '/public'));

app.use(require('body-parser').urlencoded({ extended: true }));

var dsOptions = {
  APIKey: credentials.DARKSKY_KEY,
}

var forecast = new Forecast(dsOptions);
var dsReqOptions = { 
  exclude: 'currently,minutely,daily',
};  

app.get('/', function(req, res){
  res.render('home');
});

app.post('/api/weather', function(req, res, next){

  try {
    DOMPurify.sanitize(req.body);
  } catch(err) {
    next(err);
  }
  
  var timeOfRequest = parseInt(req.body.time_of_request),
      tripDuration = parseInt(req.body.trip_duration);
      segments = req.body.weather_points;

  var waypointPromise;

  if (segments[0].length > 2) {
    waypointPromise = location.processLocations(segments, timeOfRequest, tripDuration);
  } else {
    waypointPromise = location.generateLocations(segments, timeOfRequest, tripDuration);
  }

  function getForecast(waypoint) {
    return new Promise(function(resolve, reject){
      if (!waypoint.lat || !waypoint.lng || !waypoint.duration || typeof waypoint != 'object') {
        reject(new Error('Cannot make forecast request, missing waypoint information: ', waypoint));
      }
      forecast.getAtTime(waypoint.lat, waypoint.lng, waypoint.duration, dsReqOptions, function(err, dsRes, data){
        if (err) { reject(new Error('Error processing weather results: ', err)) };
        data.waypointTime = waypoint.duration;
        resolve(data);
      });
    });
  }

  function sendResponse(resWeatherPoints) {
    res.statusCode = 200;
    res.setHeader('content-type', 'text/json');
    res.send({weather_points: resWeatherPoints});
  }

  waypointPromise
    .then(function(waypoints){   
      var forecastResponses = [];
      return waypoints.reduce(function(sequence, waypoint){
        return sequence.then(function(){
          return getForecast(waypoint);
        })
        .then(function(weatherData){
          forecastResponses.push(weatherData);
        });
      }, Promise.resolve())
      .then(function(){
        return forecastResponses;
      });
    })
    .then(function(forecastResponses) {
      return forecastRes.fillForecastResArray(forecastResponses);
    })
    .then(function(unreversedWaypoints){  
      var reversedWaypoints = [];
      return unreversedWaypoints.reduce(function(sequence, point){
        return sequence.then(function(){
          return geocoder.reverseGeocode(point.coords.lat, point.coords.lng);
        })
        .then(function(locationName){
          point.locationName = locationName;
          reversedWaypoints.push(point);
        });
      }, Promise.resolve())
      .then(function(){
        return reversedWaypoints;
      }); 
    })
    .then(function(resWeatherPoints) { sendResponse(resWeatherPoints) })
    .catch(next);

});

app.get('/about', function(req, res){
  res.render('about');
});

// custom 404 page
app.use(function(req, res){
  res.status(404);
  res.render('404');
});

// custom 500 page
app.use(function(err, req, res, next){
  console.log('we ran into an error', err);
  console.error(err.stack);
  res.status(500);
  if (req.xhr) {
    res.send({error: err});
  } else {
    res.render('500', {error: err});
  }
});

function startServer() {
	var server = app.listen(app.get('port'), function(){
		console.log( 'Express started in ' + app.get('env') + ' mode on http://localhost:' + app.get('port') + '; press Crtl-C to terminate.');
	});
}

if(require.main === module){
	// application run directly; start app startServer
	startServer();
} else {
	//application imported as a module via "require": export function to create server
	module.exports = startServer();
}